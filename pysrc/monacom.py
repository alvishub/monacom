#!/usr/bin/env python
import rospy
import math
import sys
from geometry_msgs.msg import Twist as geoTwist
from std_msgs.msg import Float32MultiArray as stdFloat32

command = geoTwist()

def main(args):
	rospy.init_node('monacom', anonymous=True)

	pubCommand = rospy.Publisher("mona0/cmd_vel", geoTwist, queue_size=1)

	rate = rospy.Rate(10) # 1-hz rate

	while not rospy.is_shutdown():
		command.linear.x = 2200
		command.angular.z = 0
		pubCommand.publish(command)
	
        rate.sleep()

if __name__ == '__main__':
	main(sys.argv)
