#!/usr/bin/env python
import rospy
import math
import serial
#import numpy as np
#import scipy as sp
import sys
import time

from geometry_msgs.msg import TransformStamped as geoTrStamp
from geometry_msgs.msg import Twist as geoTwist
from std_msgs.msg import Float32MultiArray as stdFloat32

pos = geoTrStamp()

def position_callback(pos_data):
        global pos
        pos = pos_data

def main(args):
        rospy.init_node('monamain_vicon_py', anonymous=True)
        rate = rospy.Rate(10) # 10-hz rate

        subRobotInfo = rospy.Subscriber("vicon/Ob1/Ob1", geoTrStamp, position_callback)

        rospy.loginfo("READY")
        while not rospy.is_shutdown():
                t_start = rospy.get_time()
                print_pos = '%f$ %f$ %f$' % (pos.transform.translation.x, pos.transform.translation.y, pos.transform.translation.z)
                rospy.loginfo(print_pos)
                t_now = rospy.get_time() - t_start
                rate.sleep()

if __name__ == '__main__':
	main(sys.argv)
