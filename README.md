## Monacom: ROS-based Master Computer
This package is developed for the master computer of Mona robot which has [Ardumona](https://gitlab.com/MonaCS/ardumona.git) installed on the base and [Monaros](https://gitlab.com/MonaCS/monaros.git) on its stacked Raspberry Pi Zero W.

### Preparation
1. Make sure you have a computer with Ubuntu 16.04 (or equivalent) installed. It is used as the master computer.
2. Install ROS Kinetic on your computer - follow [this instruction](http://wiki.ros.org/kinetic/Installation/Ubuntu).
3. Create a ROS workspace and build it - follow [this instruction](http://wiki.ros.org/ROS/Tutorials/InstallingandConfiguringROSEnvironment).
4. Make sure that the source is updated, use the following command to edit using Terminal.
    ```
    ~$ cd
    ~$ sudo nano .bashrc
    ```
    and then write this command at the last line of *.bashrc* file
    ```
    ~$ source ~/catkin_ws/devel/setup.bash
    ```
    Save and close the file.
5. Close the Terminal.
6. Open a new Terminal. Clone the [vicon_bridge](https://github.com/ethz-asl/vicon_bridge.git) and [Monacom](https://gitlab.com/MonaCS/monaros.git) repositories to the *src* of *catkin_ws* directory. Follow this command to do it:
    ```
    ~$ cd ~/catkin_ws/src
    ~$ git clone https://github.com/ethz-asl/vicon_bridge
    ~$ git clone https://gitlab.com/MonaCS/monacom.git
    ~$ cd ..
    ~$ catkin_make
    ```
7. Modify *datastream_hostport* in *vicon.launch* in the  *vicon_bridge/launch/* directory to comply with the IP Address of the Vicon computer. Use the following commands:
    ```
    ~$ cd ~/catkin_ws/src/vicon_bridge/launch/
    ~$ nano vicon.launch
    ```
    change the value of datastream_hostport to become
    ```
    value="<IP_ADDRESS_OF_THE_VICON_COMPUTER>:801"
    ```
    for example,
    ```
    value="192.168.1.100:801"
    ```
8. Connect to the local router where the network of Raspberry Pi Zero W and Vicon Positioning System are connected. For example, connect to a router whose SSID is *TP-LINK_F0E112* and password is *1FF0E112*.

### Operating procedures
1. Open a a new Terminal and the run a ROSMASTER using
    ```
    ~$ roscore
    ```
    and keep the Terminal open.
2. Open another new Terminal to run the Vicon launch file. Use the following command:
    ```
    ~$ roslaunch vicon_bridge vicon.launch
    ```
2. Change the mode of the python script inside the *pysrc* directory that you want to execute using the following command, e.g.,
    ```
    ~$ cd ~/catkin_ws/src/monaros/pysrc
    ~$ chmod +x monacom.py
    ```
    *if you want to execute the monacom.py*
3. Run the *monacom.py* using
    ```
    ~$ rosrun monacom monacom.py
    ```

### Communication protocol
1. To publish a command to the Raspberry Pi, the linear and angular velocity inputs should be published in the rostopic namely
    ```
    /mona0/cmd_vel
    ```
    where its declaration can be found in */monacom/pysrc/monacom.py*, i.e.,
    ```
    pubCommand = rospy.Publisher("mona0/cmd_vel", geoTwist, queue_size=1)
    ```
    If you are working with more than one robot, make sure that each robot has unique rostopic, e.g., change the rostopic in the declaration to be
    ```
    /mona1/cmd_vel
    ```
    for robot 1.
    You should publish to correct rostopic that indicate the index of each Mona robot.
    If a rosnode is publishing a message to the rostopic, you can directly check the information in a new terminal on Raspberry Pi using
    ```
    ~$ rostopic echo /mona0/cmd_vel
    ```
3. The rostopic contains a rosmessage, namely *geometry_msgs/Twist* (see [here](http://docs.ros.org/api/geometry_msgs/html/msg/Twist.html) for more details). In the case of using *monacom.py*, the linear velocity is assigned the variable
    ```
    command.linear.x
    ```
    while the angular velocity
    ```
    command.angular.z
    ```

### Reference
For more detail about ROS and Vicon, please refer to [this ROS tutorial](http://wiki.ros.org/ROS/Tutorials) and [this documentation of Vicon bridge](http://library.isr.ist.utl.pt/docs/roswiki/vicon_bridge.html).